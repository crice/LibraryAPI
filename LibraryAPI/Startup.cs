﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using JsonApiDotNetCore.Extensions;
using LibraryAPI.Models;

namespace LibraryAPI
{
    public class Startup
    {
        private readonly string ConnStr = "Server=COLIN-PC;Database=Books;Trusted_Connection=True;";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BooksContext>(options =>
            {
                options.UseSqlServer(ConnStr);
            }, ServiceLifetime.Transient);

            services.AddJsonApi<BooksContext>();
        }

        public void Configure(IApplicationBuilder app)
        {
            // Specifies using JSONAPI Core
            app.UseJsonApi();
        }
    }
}
