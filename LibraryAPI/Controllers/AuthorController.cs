﻿using JsonApiDotNetCore.Controllers;
using JsonApiDotNetCore.Services;
using LibraryAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace LibraryAPI.Controllers
{
    public class AuthorController : JsonApiController<Author>
    {
        public AuthorController(
        IJsonApiContext jsonApiContext,
        IResourceService<Author> resourceService,
        ILoggerFactory loggerFactory)
            : base(jsonApiContext, resourceService, loggerFactory)
                { }
    }
}