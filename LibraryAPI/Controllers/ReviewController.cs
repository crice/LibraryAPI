﻿using JsonApiDotNetCore.Controllers;
using JsonApiDotNetCore.Services;
using LibraryAPI.Models;
using Microsoft.Extensions.Logging;

namespace LibraryAPI.Controllers
{
    public class ReviewController : JsonApiController<Review>
    {
        public ReviewController(
        IJsonApiContext jsonApiContext,
        IResourceService<Review> resourceService,
        ILoggerFactory loggerFactory)
            : base(jsonApiContext, resourceService, loggerFactory)
                { }
    }
}