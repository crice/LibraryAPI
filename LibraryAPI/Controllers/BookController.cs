﻿using JsonApiDotNetCore.Controllers;
using JsonApiDotNetCore.Services;
using LibraryAPI.Models;
using Microsoft.Extensions.Logging;

namespace LibraryAPI.Controllers
{
    public class BookController : JsonApiController<Book>
    {
        public BookController(
        IJsonApiContext jsonApiContext,
        IResourceService<Book> resourceService,
        ILoggerFactory loggerFactory)
            : base(jsonApiContext, resourceService, loggerFactory)
                { }
    }
}