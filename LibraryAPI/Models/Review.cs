﻿using JsonApiDotNetCore.Models;

namespace LibraryAPI.Models
{
    public partial class Review : Identifiable
    {
        #region Exposed Attributes
        [Attr("reviewer-name")]
        public string ReviewerName { get; set; }
        [Attr("body")]
        public string Body { get; set; }
        [Attr("book-id")]
        public int BookId { get; set; }
        #endregion

        public override int Id { get; set; }
        
        // Specifying entity relationships
        public Book Book { get; set; }
    }
}
