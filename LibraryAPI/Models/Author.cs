﻿using JsonApiDotNetCore.Models;
using System.Collections.Generic;

namespace LibraryAPI.Models
{
    public partial class Author : Identifiable
    {
        #region Exposed Attributes
        [Attr("first-name")]
        public string FirstName { get; set; }
        [Attr("last-name")]
        public string LastName { get; set; }
        #endregion

        public override int Id { get; set; }

        // Specifying entity relationship
        [HasMany("books")]
        public List<Book> Books { get; set; }
    }
}
