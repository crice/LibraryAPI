﻿using JsonApiDotNetCore.Models;
using System.Collections.Generic;

namespace LibraryAPI.Models
{
    public partial class Book : Identifiable
    {
        #region Exposed Attributes
        [Attr("title")]
        public string Title { get; set; }
        [Attr("isbn")]
        public int Isbn { get; set; }
        [Attr("publish-date")]
        public string PublishDate { get; set; }
        [Attr("author-id")]
        public int AuthorId { get; set; }
        #endregion

        public override int Id { get; set; }

        // Specifying entity relationships
        public Author Author { get; set; }
        [HasMany("reviews")]
        public List<Review> Reviews { get; set; }
    }
}
